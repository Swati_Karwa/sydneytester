package parallelbrowser.based;

import java.util.concurrent.TimeUnit;
import org.junit.After;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import pages.BuyInsurancePageFactory;
import pages.GetQuotePageFactory;
import pages.HomePageFactory;
import pages.PaymentPageFactory;

	public class BookCarInsParallelTest {
		private WebDriver driver;
		String baseURL = "http://sydneytesters.herokuapp.com/";
		HomePageFactory homePage;
		GetQuotePageFactory getQuotePage;
		BuyInsurancePageFactory buyInsPage;
		PaymentPageFactory payPage;

        @Parameters("browser")
		@BeforeTest
		public void openBrowser(String browser) {
        	try {
    			if (browser.equalsIgnoreCase("Firefox")) {
    				driver = new FirefoxDriver();
    			} else if (browser.equalsIgnoreCase("chrome")) {
    				System.setProperty("webdriver.chrome.driver", "browser//chromedriver.exe");
    				driver = new ChromeDriver();
    			} 
    		
    		} catch (WebDriverException e) {
    			System.out.println(e.getMessage());
    		}
			
			
			homePage = new HomePageFactory(driver) ;
			getQuotePage = new GetQuotePageFactory(driver); 
			buyInsPage = new BuyInsurancePageFactory(driver);
			payPage = new PaymentPageFactory(driver);

			// Maximize the browser's window
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.get(baseURL);
		}
        
@Test
 
        	public BookCarInsParallelTest() throws InterruptedException{
    		homePage.clickGetCarQuoteButton();
    		getQuotePage.clickMakeDropDown();
    		getQuotePage.makeViaDropDown(2);
    		getQuotePage.setModelYear("2013");
    		getQuotePage.setDriverAge("27");
    		getQuotePage.clickgenderRadioButton();
    		getQuotePage.clickStateDropdown();
    		getQuotePage.stateViaDropDown(2);
    		getQuotePage.setEmailId("john@gmail.com");
    		getQuotePage.clickGetQuoteButton();
    		buyInsPage.clickBuyInsuranceButton();
    		payPage.setNameOnCard("John B");
    		payPage.setEmailUserName("Abc@gmail.com");
    		payPage.setPassword("test123@");
    		payPage.setCardNumber("4111111111111111");
    		payPage.clickExpiryMonthDropdown();
    		payPage.expiryMonthViaDropDown(2);
    		payPage.clickExpiryYearDropdown();
    		payPage.expiryYearViaDropDown(5);
    		payPage.setCardCVV("343");
    		payPage.clickPayNowButton();
    		driver.quit();
    		
    	}
    	@After
    	public void afterClass() {
    	}

    	public void close() {
    		driver.quit();
    		}
    }
