package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePageFactory {
	WebDriver driver;

	/**
	 * 
	 * WebElements are identified by @FindBy annotation
	 * 
	 */

	@FindBy(name = "getcarquote")
	WebElement getCarQuoteButton;

	// This initElements method will create all WebElements

	public HomePageFactory(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public void clickGetCarQuoteButton() {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement getCarQuoteButton = wait.until(ExpectedConditions.elementToBeClickable(By.id("getcarquote")));
		getCarQuoteButton.click();
	}

}