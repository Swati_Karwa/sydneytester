package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class PaymentPageFactory {
	WebDriver driver;

	@FindBy(id = "cardholdername")
	WebElement nameOnCard;

	@FindBy(xpath = "/html/body/div/div[3]/div[2]/div/div/div/div/div[2]/form/fieldset/div[2]/div/input")
	WebElement emailUserName;

	@FindBy(xpath = "/html/body/div/div[3]/div[2]/div/div/div/div/div[2]/form/fieldset/div[3]/div/input")
	WebElement password;

	@FindBy(id = "card-number")
	WebElement cardNumber;

	@FindBy(id = "expiry-month")
	WebElement expiryMonthDropdown;

	@FindBy(name = "expiry-year")
	private WebElement expiryYeardropdown;

	@FindBy(name = "cvv")
	private WebElement cardCVV;

	@FindBy(xpath = "//button[@id='pay']")
	private WebElement payNowButton;

	public PaymentPageFactory(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public void setNameOnCard(String name) {
		nameOnCard.sendKeys(name);
	}

	public void setEmailUserName(String username) {
		emailUserName.sendKeys(username);
	}

	public void setPassword(String paswr) {
		password.sendKeys(paswr);
	}

	public void setCardNumber(String cardNo) {
		cardNumber.sendKeys(cardNo);
	}

	public void clickExpiryMonthDropdown() {
		expiryMonthDropdown.click();
	}

	public void expiryMonthViaDropDown(int index) {
		Select drop = new Select(expiryMonthDropdown);
		drop.selectByIndex(index);
	}

	public void clickExpiryYearDropdown() {
		expiryYeardropdown.click();
	}

	public void expiryYearViaDropDown(int index) {
		Select drop = new Select(expiryYeardropdown);
		drop.selectByIndex(index);
	}

	public void setCardCVV(String cvv) {
		cardCVV.sendKeys(cvv);
	}

	public void clickPayNowButton() {
		payNowButton.click();
	}
}
