package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class BuyInsurancePageFactory {
	WebDriver driver;

	@FindBy(xpath = "//a[@id='payment']")
	WebElement buyInsuranceButton;
	
	@FindBy(xpath = "//a[@id='payment']")
	WebElement buyInsuranceButtonText;

	public BuyInsurancePageFactory(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public void clickBuyInsuranceButton() {
		buyInsuranceButton.click();
	}
	 

	public String getBuyInsuranceButtonTitle() {
		return    buyInsuranceButtonText.getText();
	}

}