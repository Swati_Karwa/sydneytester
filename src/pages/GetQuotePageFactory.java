package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class GetQuotePageFactory {
	

	WebDriver driver;

	@FindBy(id = "make")
	WebElement makeDropdown;

	@FindBy(id = "year")
	WebElement modelYear;

	@FindBy(id = "age")
	WebElement driverAge;

	@FindBy(id = "email")
	WebElement emailId;

	@FindBy(id = "female")
	WebElement genderRadioButton;

	@FindBy(name = "state")
	WebElement stateDropdown;

	@FindBy(xpath = "//*[@id='getquote']")
	WebElement getQuoteButton;

	public GetQuotePageFactory(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public void clickMakeDropDown() {
		makeDropdown.click();
	}

	public void makeViaDropDown(int index) {
		Select drop = new Select(makeDropdown);
		drop.selectByIndex(index);
	}

	public void setModelYear(String year) {
		modelYear.sendKeys(year);
	}

	public void setDriverAge(String age) {
		driverAge.sendKeys(age);
	}

	public void clickgenderRadioButton() {
		genderRadioButton.click();

	}

	public void clickStateDropdown() {
		stateDropdown.click();
	}

	public void stateViaDropDown(int index) {
		Select drop = new Select(stateDropdown);
		drop.selectByIndex(index);
	}

	public void setEmailId(String email) {
		emailId.sendKeys(email);
	}

	public void clickGetQuoteButton() {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement getQuoteButton = wait.until(ExpectedConditions.elementToBeClickable(By.id("getquote")));
		getQuoteButton.click();
	}
}
