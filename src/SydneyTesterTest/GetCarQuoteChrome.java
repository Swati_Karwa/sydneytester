package SydneyTesterTest;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeClass;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import pages.BuyInsurancePageFactory;
import pages.GetQuotePageFactory;
import pages.HomePageFactory;
import pages.PaymentPageFactory;

public class GetCarQuoteChrome {
	WebElement element;
	WebDriver driver;
	String baseUrl;
	HomePageFactory homePage;
	GetQuotePageFactory getQuotePage;
	BuyInsurancePageFactory buyInsPage;
	PaymentPageFactory payPage;

	@BeforeClass
	public static void classSetUp() {
		System.setProperty("webdriver.chrome.driver", "browser//chromedriver.exe");
	}

	@BeforeMethod
	public void setUp() throws Exception {
		driver = new ChromeDriver();
		baseUrl = "http://sydneytesters.herokuapp.com/";
		homePage = new HomePageFactory(driver);
		getQuotePage = new GetQuotePageFactory(driver);
		buyInsPage = new BuyInsurancePageFactory(driver);
		payPage = new PaymentPageFactory(driver);

		// Maximize the browser's window
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get(baseUrl);

	}

	@Test
	public void test() throws Exception {
		homePage.clickGetCarQuoteButton();
		getQuotePage.clickMakeDropDown();
		getQuotePage.makeViaDropDown(2);
		getQuotePage.setModelYear("2015");
		getQuotePage.setDriverAge("32");
		getQuotePage.clickgenderRadioButton();
		getQuotePage.clickStateDropdown();
		getQuotePage.stateViaDropDown(2);
		getQuotePage.setEmailId("john@gmail.com");
		getQuotePage.clickGetQuoteButton();
		Assert.assertEquals("Buy Insurance", driver.findElement(By.id("payment")).getText());
		driver.quit();

	}

	@AfterMethod
	public void afterClass() {
	}

	public void close() {
		driver.quit();
	}

}
