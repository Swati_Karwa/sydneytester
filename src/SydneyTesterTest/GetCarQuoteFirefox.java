package SydneyTesterTest;

import java.util.concurrent.TimeUnit;
import org.testng.annotations.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import pages.BuyInsurancePageFactory;
import pages.GetQuotePageFactory;
import pages.HomePageFactory;
import pages.PaymentPageFactory;

public class GetCarQuoteFirefox {

	WebDriver driver;
	WebElement element;
	String baseUrl;
	HomePageFactory homePage;
	GetQuotePageFactory getQuotePage;
	BuyInsurancePageFactory buyInsPage;
	PaymentPageFactory payPage;

	@BeforeMethod
	// public void beforeClass()
	public void setUp() throws Exception {
		driver = new FirefoxDriver();
		baseUrl = "http://sydneytesters.herokuapp.com/";
		homePage = new HomePageFactory(driver);
		getQuotePage = new GetQuotePageFactory(driver);
		buyInsPage = new BuyInsurancePageFactory(driver);
		payPage = new PaymentPageFactory(driver);

		// Maximize the browser's window
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get(baseUrl);
	}

	@Test
	public void test() throws Exception {
		// get car quote page will open
		homePage.clickGetCarQuoteButton();
		// enter details to get the car quote
		getQuotePage.clickMakeDropDown();
		getQuotePage.makeViaDropDown(2);
		getQuotePage.setModelYear("2015");
		getQuotePage.setDriverAge("28");
		getQuotePage.clickgenderRadioButton();
		getQuotePage.clickStateDropdown();
		getQuotePage.stateViaDropDown(2);
		getQuotePage.setEmailId("john@gmail.com");
		getQuotePage.clickGetQuoteButton();
		// car quote will be displayed and you can buy it
		Assert.assertEquals("Buy Insurance", driver.findElement(By.id("payment")).getText());
		// String BuyInsuranceButtonTitle =
		// buyInsPage.getBuyInsuranceButtonTitle();
		// Assert.assertFalse(BuyInsuranceButtonTitle.toLowerCase().contains("Buy
		// Insurance"));
		driver.quit();

	}

	@AfterMethod
	public void afterClass() {
	}

	public void close() {
		driver.quit();
	}

}
